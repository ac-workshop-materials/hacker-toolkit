var tools = require('./tools');
var { rot13 } = require('crypto-toolkit');

module.exports = {
    rot13,
    spy: tools.asyncCracker.spy,
    bruteForce: tools.asyncCracker.bruteForce,
    editSudoers: tools.asyncCracker.editSudoers,
    addSuperUser: tools.syncCracker.addToSudoers,
};
