var terminalInternalKey = 'codigoergosum';

var messages = require('../internals/messages');


exports.bruteForce = function(terminal, accounts, next) {

    var ATTEMPT_INTERVAL_SECONDS = 0.5;

    if (!terminal.login) {
        messages.stderr(messages.BRUTEFORCE_NAME, messages.NOT_A_TERMINAL);
        return;
    }

    var accountIndex = 0;
    var id = setInterval(function() {

        var account, success;

        account = accounts[accountIndex++];

        if (!account) {
            messages.stderr(messages.BRUTEFORCE_NAME, 'Could not find a valid account in the list..');
            clearInterval(id);
            return;
        }

        if (!account.username || !account.password) {
            messages.stderr(messages.BRUTEFORCE_NAME, messages.ACCOUNT_NOT_VALID);
            return;
        }

        messages.stdout(messages.BRUTEFORCE_NAME, 'Testing account ' + account.username + ':' + account.password);
        success = terminal.isValid(terminalInternalKey, account.username, account.password);

        if (success) {
            messages.success(messages.BRUTEFORCE_NAME, 'Found working credentials!');
            clearInterval(id);
            next(account.username, account.password);
        }
    }, ATTEMPT_INTERVAL_SECONDS * 1000);
};

// this function... wow... just wow.. please organize this mess soon
exports.spy = function(terminal, username, next) {

    var interrupted;

    if (!terminal.login) {
        messages.stderr(messages.SPY_NAME, messages.NOT_A_TERMINAL);
        return;
    }

    if (!terminal.isLoggedIn()) {
        messages.stderr(messages.SPY_NAME, 'You are not logged in! Aborting ActionSpy!');
        return;
    }

    if (username !== 'root') {
        messages.stderr(messages.SPY_NAME, username + ' is not logged in, aborting ActionSpy.');
        return;
    }

    terminal.subscribe(terminalInternalKey, function(action) {
        if (action === 'printFile') {
            interrupted = true;
        }
    });

    terminal.writeToStdIn(terminalInternalKey, 'run spy-action.exe');
    messages.stdout(messages.SPY_NAME, 'Listening to ' + username + ' actions....');
    fakeActions(messages.RANDOM_ACTIONS);

    function fakeActions(actions, interval) {

        interval = interval || (Math.random() + 3) * 1000;

        if (actions.length <= 0) {
            return;
        }

        setTimeout(function() {

            if (!terminal.isLoggedIn()) {
                messages.stderr(messages.SPY_NAME, 'You are not logged in! Aborting ActionSpy!');
                return;
            }

            if (interrupted) {
                messages.stderr(messages.SPY_NAME, 'You cant execute commands while ActionSpy is executing. Aborting...');
                return;
            }

            var randomAction = actions.pop();
            terminal.writeToStdOut(terminalInternalKey, '[ACTION CAPTURED]');

            interval = (Math.random() * 3 + 1) * 1000;
            fakeActions(actions, interval);
            next(randomAction);
        }, interval);
    }
};

exports.editSudoers = function(terminal, next) {

    if (!terminal.login) {
        messages.stderr(messages.EXPLOITER_NAME, messages.NOT_A_TERMINAL);
        return;
    }

    var tries = Math.floor(Math.random() * 5) + 3; // TODO: no magic numbers, please. this just generates a number between 3 and 8
    var inside = false;

    terminal.writeToStdIn(terminalInternalKey, messages.EXECUTE_EXPLOITER);

    var id = setInterval(function() {

        if (!terminal.loggedIn(terminalInternalKey)) {
            messages.stderr(messages.EXPLOITER_NAME, messages.NOT_LOGGED_IN_TERMINAL);
            clearInterval(id);
            return;
        }

        if (!inside) {
            messages.success(messages.EXPLOITER_NAME, messages.INSIDE_TERMINAL);
            inside = true;
            return;
        }

        if (!tries) {
            clearInterval(id);

            var superUsers = terminal.superUsers(terminalInternalKey);

            messages.success(messages.EXPLOITER_NAME, messages.VULNERABILITY_FOUND);
            messages.success(messages.EXPLOITER_NAME, messages.SUDOERS_FILE_CRACKED);
            console.log();

            next(superUsers);
            return;
        }

        messages.stdout(messages.EXPLOITER_NAME, messages.SEARCHING_VULNERABILITIES);
        tries--;

    }, 1000);

};
