var terminalInternalKey = 'codigoergosum'; // todo hide this somehow, and link it with terminal

var messages = require('../internals/messages');
var asyncSleep = require('sleep-promise');
var syncSleep = require('sleep');

var DEFAULT_BLOCK_SECONDS = 0.1;
var HOW_MANY_ATTEMPTS = 5;

exports.addToSudoers = function(terminal, username) {
    // quick test to check if it is a terminal. please check this later
    if (!terminal.login) {
        messages.stderr(messages.EXPLOITER_NAME, messages.NOT_A_TERMINAL);
        return;
    }

    var inside = false;
    var superUsers;

    terminal.writeToStdIn(terminalInternalKey, messages.EXECUTE_EXPLOITER);

    if (!terminal.isLoggedIn()) {
        messages.stderr(messages.EXPLOITER_NAME, messages.NOT_LOGGED_IN_TERMINAL);
        return;
    }

    messages.success(messages.EXPLOITER_NAME, messages.INSIDE_TERMINAL);

    for (var i = 0; i < HOW_MANY_ATTEMPTS; i++) {
        messages.stderr(messages.EXPLOITER_NAME, messages.SEARCHING_VULNERABILITIES);
        block();
    }

    superUsers = terminal.superUsers(terminalInternalKey);
    superUsers.push(username);

    messages.success(messages.EXPLOITER_NAME, messages.VULNERABILITY_FOUND);
    block();
    messages.success(messages.EXPLOITER_NAME, messages.SUDOERS_FILE_CRACKED);
    console.log();
    block();
};

function block(seconds) {
    seconds = seconds || DEFAULT_BLOCK_SECONDS;

    syncSleep.msleep(seconds * 1000);
}
