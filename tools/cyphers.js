var { rot13 } = require('crypto-toolkit');
var messages = require('../internals/messages');

exports.rot13 = function(text) {
    var rotted = rot13(text);

    // hmmm maybe not..?
    //messages.stdout(messages.ROT13_NAME, '%s | rot-13-crypt   >>', [text, rotted]);

    return rotted;
};
