var chalk = require('chalk');
const { getRootPassword } = require('crypto-toolkit');

var styles = {
    default: chalk.black.bold.bgWhite,
    stdout: chalk.white,
    stderr: chalk.white.bold.bgRed,
    success: chalk.black.bold.bgGreen
};

exports.EXPLOITER_NAME = 't3rminal-3xpl0it3r';
exports.ROT13_NAME = 'r0t-13 decrypt';
exports.BRUTEFORCE_NAME = 'BR00T-F0RCE';
exports.SPY_NAME = 'action$py';
exports.NOT_A_TERMINAL = 'The first argument is not a terminal... Please give me one I can crack.';
exports.EXECUTE_EXPLOITER = 'run ~/terminal-exploiter.exe\n';
exports.NOT_LOGGED_IN_TERMINAL = 'You are not logged in the terminal. Please login first!';
exports.INSIDE_TERMINAL = 'Hijacked current terminal session!';
exports.SEARCHING_VULNERABILITIES = 'Searching for vulnerabilities, please wait...';
exports.VULNERABILITY_FOUND = 'Vulnerability has been found!';
exports.SUDOERS_FILE_CRACKED = 'SUDOERS file has been cracked. Editing file...';
exports.ACCOUNT_NOT_VALID = 'The provided account is not valid. Use an object with a username and password property.';
exports.RANDOM_ACTIONS = [
    `changed password to ${getRootPassword()}`, 
    'writing to file /var/logs/ecorp.txt',
    'moved to folder /var/logs/',
    'killed process 1452',
    'listing processes',
];

exports.styles = styles;

exports.message = function(chalkStyle, name, msg, variables) {

    msg = [styles.default(' ' + name + ' ') + ' > ' + chalkStyle(msg)];

    if (variables) {
        msg = msg.concat(variables);
    }

    console.log.apply(null, msg);
};

exports.stderr = function(name, msg, variables) {
    exports.message(styles.stderr, name, msg, variables);
};

exports.stdout = function(name, msg, variables) {
    exports.message(styles.stdout, name, msg, variables);
};

exports.success = function(name, msg, variables) {
    exports.message(styles.success, name, msg, variables);
};
